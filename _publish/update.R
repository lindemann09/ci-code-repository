# Call this file to update your code repository

# Helper script to update students local code repository during R courses
#
# Instruction for new courses
#   1. Adapt .user and .repository
#   2. provide this file as download
#   3. students have to download and call this file
#
# Version 0.4.2
#
# (c) Oliver Lindemann

.user = "lindemann09"
.git_repository = "RP-41BC-21"
.publish_folder = "_publish"
.code_folder = "repository"

.download_gitlab <- function(git_repository, user, zip_file, override=FALSE) {

  # download a commit
  destination_zip = paste0("tmp_repository.zip")
  giturl = paste0("http://", user, ".gitlab.io/", git_repository, "/", zip_file)

  download.file(giturl, destfile = destination_zip)

  suppressWarnings(
    unzip(destination_zip, overwrite = override)
  )
  file.remove(destination_zip)
}


.download_gitlab(git_repository=.git_repository, user=.user,
				 zip_file =paste0(.code_folder, ".zip"))

# copy update.R to base dir and rm _publish folder
file.copy(file.path(.code_folder, .publish_folder, "update.R"),
          file.path(".", "update.R"), overwrite=TRUE)
unlink(file.path(.code_folder, .publish_folder), recursive = TRUE)

# copy all files and folder in code folder to base dir
for (x in list.files(path = .code_folder, full.names = FALSE,
							recursive = FALSE)) {
  file.copy(from = file.path(.code_folder, x), to = ".",
            overwrite = TRUE, recursive = TRUE, copy.mode = TRUE)
}
rm(x)
# remove code folder
unlink(.code_folder, recursive = TRUE)
